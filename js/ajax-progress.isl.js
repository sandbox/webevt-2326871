/**
 * @file Contains 'isLoading' progress type handler.
 */

(function ($) {

  if ($.fn.isLoading) {
    var defaultOptions = {
      'position': 'overlay',        // right | inside | overlay
      'text': Drupal.t('Loading'),  // text to display next to the loader
      'class': 'icon-refresh',      // loader CSS class
      'tpl': '<span class="isloading-wrapper %wrapper%"><div class="throbber"></div>%text%</span>',
      'disableSource': true,        // true | false
      'disableOthers': []
    };

    /**
     * Check if element has isLoading plugin initialized.
     *
     * @param {jQuery} obj
     * @returns {*}
     */
    function isInitialized(obj) {
      return $.data(obj, "plugin_isLoading");
    }

    /**
     * Success & error callback.
     *
     * @param e
     * @param ajax
     */
    function successErrorCallback(e, ajax) {
      var $cnt = this.getContainer();
      $.each($cnt, function (i, el) {
        if (isInitialized(el)) {
          $cnt.isLoading('hide');
        }
      });
    }

    // Add progress type plugin.
    AjaxProgress.addProgressType('isLoading', {
      beforeSend: function (e, ajax, xhr, options) {
        this.getContainer()
          .isLoading($.extend(true, {}, defaultOptions, ajax.progress.options));
      },

      success: successErrorCallback,
      error: successErrorCallback
    });
  }

})(jQuery);
