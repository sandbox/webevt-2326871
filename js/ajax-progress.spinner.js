/**
 * @file Contains 'spinner' progress type handler.
 */

(function ($) {

  if (window.Spinner) {
    var defaultOptions = {};

    /**
     * Success & error callback.
     *
     * @param e
     * @param ajax
     */
    function successErrorCallback(e, ajax) {
      var $cnt = this.getContainer();
      $.each($cnt, function (i, el) {
        $(el).spin(false);
      });
    }

    // Add progress type plugin.
    AjaxProgress.addProgressType('spinner', {
      beforeSend: function (e, ajax, xhr, options) {
        this.getContainer().spin($.extend(true, {}, defaultOptions, ajax.progress.options));
      },

      success: successErrorCallback,
      error: successErrorCallback
    });
  }

})(jQuery);
