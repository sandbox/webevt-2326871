/**
 * @file Contains AjaxProgress class and overrides Drupal.ajax methods.
 */

/**
 * AjaxProgress class.
 *
 * @param ajax
 * @constructor
 */
function AjaxProgress(ajax) {
  this.ajax = ajax;

  this.attachEvents();
}

(function ($) {
  // Save default handlers.
  var beforeSend = Drupal.ajax.prototype.beforeSend;
  var success = Drupal.ajax.prototype.success;
  var error = Drupal.ajax.prototype.error;

  /**
   * Prepare the Ajax request before it is sent.
   */
  Drupal.ajax.prototype.beforeSend = function (xmlhttprequest, options) {
    // Call default beforeSend handler.
    beforeSend.call(this, xmlhttprequest, options);

    var type = this.progress && this.progress.type ? this.progress.type : false;
    if (type && type != 'none') {
      /**
       * Trigger ajaxProgress event to give interactors ability to create
       * progress indicator.
       */
      $(this.element).trigger('ajaxProgress:beforeSend', [this, xmlhttprequest, options]);
    }
  };

  /**
   * Handler for the form redirection completion.
   */
  Drupal.ajax.prototype.success = function (response, status) {
    // Call default success handler.
    success.call(this, response, status);

    var type = this.progress && this.progress.type ? this.progress.type : false;
    if (type && type != 'none') {
      /**
       * Trigger ajaxProgress:success event to give interactors ability to
       * react on request success.
       */
      $(this.element).trigger('ajaxProgress:success', [this, response, status]);
    }
  };

  /**
   * Handler for the form redirection error.
   */
  Drupal.ajax.prototype.error = function (response, uri) {
    // Call default error handler.
    error.call(this, response, uri);

    var type = this.progress && this.progress.type ? this.progress.type : false;
    if (type && type != 'none') {
      /**
       * Trigger ajaxProgress:error event to give interactors ability to react
       * on request error.
       */
      $(this.element).trigger('ajaxProgress:error', [this, response, uri]);
    }
  };

  /**
   * Add new progress type handler.
   *
   * @param type
   * @param callbacks
   */
  AjaxProgress.addProgressType = function (type, callbacks) {
    var defaultCallbacks = {
      beforeSend: function (e, ajax, xhr, options) {
      },
      success: function (e, ajax, response, status) {
      },
      error: function (e, ajax, response, uri) {
      }
    };

    AjaxProgress.prototype.progressTypes[type] = $.extend(true, {}, defaultCallbacks, callbacks);
  };

  /**
   * AjaxProgress prototype.
   */
  AjaxProgress.prototype = {
    // Stores Drupal.ajax object.
    ajax: false,
    // Stores progressType handlers.
    progressTypes: {},

    /**
     * Attach events to a Drupal.ajax element.
     */
    attachEvents: function () {
      var me = this;
      var $el = $(me.ajax.element);
      $el.once('ajax-progress')
        .bind('ajaxProgress:beforeSend ajaxProgress:success ajaxProgress:error', function (e, ajax) {
          // Get event type: beforeSend, success or error.
          var eventType = e.type.split(':')[1];
          // Get callback name and call it.
          var callback = me.getCallback(me.getType(), eventType);
          if (typeof callback == 'function') {
            callback.apply(me, arguments);
          }
        });
    },

    /**
     * Get progress type.
     *
     * @returns {String|boolean}
     */
    getType: function () {
      if (this.ajax.progress && this.ajax.progress.type) {
        return this.ajax.progress.type;
      }
      return false;
    },

    /**
     * Get progress container.
     *
     * @returns {jQuery|false}
     *    Returns jQuery collection of containers.
     */
    getContainer: function () {
      var $cnt = $();
      var container = this.ajax.progress.container;
      if (!container) {
        container = this.ajax.wrapper;
      }

      // It is possible that there are many containers.
      if ($.isArray(container)) {
        $.each(this.ajax.progress.container, function (i, selector) {
          $cnt = $cnt.add($(selector));
        });
      }
      else {
        $cnt = $cnt.add($(this.ajax.progress.container));
      }

      return $cnt;
    },

    /**
     * Get progress type callback for given event.
     * @param type
     * @param event
     * @returns {*}
     */
    getCallback: function (type, event) {
      if (this.progressTypes[type]) {
        return this.progressTypes[type][event];
      }
      return false;
    }
  };

  /**
   * @type {{attach: attach}}
   */
  Drupal.behaviors.ajaxProgress = {
    attach: function (context) {
      $.each(Drupal.ajax, function (key, ajax) {
        new AjaxProgress(ajax);
      });
    }
  };

})(jQuery);
